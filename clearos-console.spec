Name: clearos-console
Version: 7.6.3
Release: 1%{dist}
Summary: Administration console module
License: GPLv3 or later
Group: System Environment/Base
Source: %{name}-%{version}.tar.gz
Vendor: ClearFoundation
Requires: system-base
Requires: iptraf
Requires: kbd
Requires: tconsole >= 3.3-11
Requires: ethtool
Requires: systemd
Requires: sudo
BuildArch: noarch

%description
Administration console module

%prep
%setup -q
%build

%install
mkdir -p -m 755 $RPM_BUILD_ROOT/etc/systemd/system/getty@tty1.service.d
install -m 644 autologin.conf $RPM_BUILD_ROOT/etc/systemd/system/getty@tty1.service.d/

mkdir -p -m 755 $RPM_BUILD_ROOT/var/lib/clearconsole
install -m 644 bash_profile $RPM_BUILD_ROOT/var/lib/clearconsole/.bash_profile

mkdir -p -m 755 $RPM_BUILD_ROOT/etc/sudoers.d
install -m 644 clearos-console-sudoers $RPM_BUILD_ROOT/etc/sudoers.d/clearos-console

%pre
getent group clearconsole >/dev/null || groupadd -r clearconsole
getent passwd clearconsole >/dev/null || \
    useradd -r -g clearconsole -d /var/lib/clearconsole/ -s /bin/bash \
    -c "Console" clearconsole
exit 0

%post

CHECK=`grep -i clearconsole /etc/sudoers 2>/dev/null`
if [ -n "$CHECK" ]; then
    sed -i -e '/clearconsole/Id' /etc/sudoers
fi
exit 0

%files
%defattr(-,root,root)
/etc/systemd/system/getty@tty1.service.d
/var/lib/clearconsole/.bash_profile
%attr(0640,root,root) /etc/sudoers.d/clearos-console
%dir %attr(-,clearconsole,root) /var/lib/clearconsole
